#
# Be sure to run `pod lib lint GZCExtends.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GZCExtends'
  s.version          = '0.2.5'
  s.summary          = '扩展功能模块'

  s.description      = <<-DESC
TODO: 集成了SwifterSwift中的相关扩展，并增加了一些自己用的扩展方法
                       DESC

  s.homepage         = 'https://gitlab.com/GZCFrameworks_swift/GZCExtends'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guo ZhongCheng' => 'gzhongcheng@qq.com' }
  s.source           = { :git => 'https://gitlab.com/GZCFrameworks_swift/GZCExtends.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'
  s.swift_version = "5.1"
  
  s.default_subspec = "All"
  
  s.subspec "Log" do |ss|
    ss.source_files  = "GZCExtends/Classes/Log.swift"
  end
  
  s.subspec "AppKit" do |ss|
    ss.source_files  = "GZCExtends/Classes/AppKit/**/*.swift"
    ss.dependency "SwifterSwift/AppKit"
  end
  
  s.subspec "CoreGraphics" do |ss|
    ss.source_files  = "GZCExtends/Classes/CoreGraphics/**/*.swift"
    ss.dependency "SwifterSwift/CoreGraphics"
  end
  
  s.subspec "Dispatch" do |ss|
    ss.source_files  = "GZCExtends/Classes/Dispatch/**/*.swift"
    ss.dependency "SwifterSwift/Dispatch"
  end
  
  s.subspec "SwiftStdlib" do |ss|
    ss.source_files  = "GZCExtends/Classes/SwiftStdlib/**/*.swift"
    ss.dependency "SwifterSwift/SwiftStdlib"
  end
  
  s.subspec "UIKit" do |ss|
    ss.source_files  = "GZCExtends/Classes/UIKit/**/*.swift"
    ss.dependency "SwifterSwift/UIKit"
  end
  
  s.subspec "WebImage" do |ss|
    ss.source_files  = "GZCExtends/Classes/WebImage/**/*.swift"
    ss.dependency 'Kingfisher'
    ss.dependency 'KingfisherWebP'
    ss.dependency 'GZCExtends/Dispatch'
  end
  
  s.subspec "All" do |ss|
    ss.dependency 'GZCExtends/Log'
    ss.dependency 'GZCExtends/AppKit'
    ss.dependency 'GZCExtends/CoreGraphics'
    ss.dependency 'GZCExtends/Dispatch'
    ss.dependency 'GZCExtends/SwiftStdlib'
    ss.dependency 'GZCExtends/UIKit'
    ss.dependency 'GZCExtends/WebImage'
    ss.dependency "SwifterSwift/Foundation"
    ss.dependency "SwifterSwift/CoreLocation"
    ss.dependency "SwifterSwift/CoreAnimation"
    ss.dependency "SwifterSwift/MapKit"
    ss.dependency "SwifterSwift/StoreKit"
  end
  
  ### 目前没用到 SceneKit和SpriteKit
#  ss.dependency "SwifterSwift/SpriteKit"
#  ss.dependency "SwifterSwift/SceneKit"
  
end
