//
//  FloatExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

import SwifterSwift

// MARK: - 属性
public extension Float {
    
    /// 转字符串(两位小数)
    var string: String {
        return String(format: "%.2f", self)
    }
}
