//
//  IntExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

import Foundation

// MARK: - Properties
public extension Int {
    
    /// 转字符串
    var string: String {
        return String(format: "%d", self)
    }
    
    /// 按10000格式化 ±1000 (如: 1w, -2w, 100w, 1kw, -5kw..)
    var wFormatted: String {
        var sign: String {
            return self >= 0 ? "" : "-"
        }
        let abs = Swift.abs(self)
        if abs == 0 {
            return "0"
        } else if abs >= 0 && abs < 10000 {
            return String(format: "\(sign)%i", abs)
        } else if abs >= 10000 && abs < 10000000 {
            return String(format: "\(sign)%.1fw", Float(abs) / 10000.0)
        }
        return String(format: "\(sign)%.1fkw", Float(abs) / 10000000.0)
    }

}
