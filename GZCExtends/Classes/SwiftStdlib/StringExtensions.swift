//
//  StringExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

@_exported import SwifterSwift
import SwifterSwift

// MARK:- url参数解析
public extension String {
    /// 解析URL链接中的参数字典
    var urlParameters: [String: String] {
        var dic = [String: String]();
        guard
            let index = self.lastIndex(of: "?"),
            let range = self.range(of: self)
        else {
            return dic
        }
        let paramsString = self.slicing(at: self.distance(from: range.lowerBound, to: index) + 1)!
        let queryArray = (paramsString.components(separatedBy: "&")) as Array<String>
        
        for index in 0 ..< queryArray.count {
            let queryComponent = queryArray[index]
            let compArr = queryComponent.components(separatedBy: "=") as Array<String>
            if compArr.count >= 2 {
                let key = compArr[0]
                let val = compArr[1].removingPercentEncoding
                dic.updateValue(val ?? "", forKey: key)
            }
        }
        return dic
    }
    
}

// MARK:- 方法
public extension String {
    #if os(iOS) || os(macOS)
    /// 生成指定符号的价格富文本，默认符号为人民币符号
    func priceAttributes(_ unitFont: UIFont, unitString: String = "¥") -> NSAttributedString {
        let mutStr = NSMutableAttributedString(string: "\(unitString) \(self)")
        mutStr.addAttributes([
            .font: unitFont,
            .baselineOffset: -0.5
        ], range: NSRange(location: 0, length: unitString.count + 1))
        return mutStr
    }
    #endif

    
    #if canImport(AppKit) || canImport(UIKit)
    /// 生成指定子字符串为指定颜色的富文本
    ///
    /// - Parameter color: 文本颜色
    /// - Parameter string: 要指定颜色的子字符串
    /// - Returns: 带指定颜色的富文本
    func colored(with color: UIColor,string: String) -> NSMutableAttributedString {
        let mutStr = NSMutableAttributedString(string: self)
        mutStr.addAttribute(.foregroundColor, value: color, range: self.rangeOf(string))
        return mutStr
    }
    #endif
    
    /// 查找指定字符串的范围
    /// - Parameter string: 指定字符串
    func rangeOf(_ string: String) -> NSRange {
        guard let range = self.range(of: string) else {
            return NSMakeRange(0, 0)
        }
        let star_idx:Int = self.distance(from: self.startIndex, to: range.lowerBound);
        let length:Int = self.distance(from: self.startIndex, to: range.upperBound) - star_idx;
        return NSMakeRange(star_idx, length)
    }
    
    /// 从指定位置开始裁剪字符串
    ///
    ///        var str = "Hello World"
    ///        print(str.slicing(at: 6)) // prints "World"
    ///
    /// - Parameter index: 裁剪起点
    func slicing(at index: Int) -> String? {
        guard index < count else { return self }
        return self[safe: index..<count]
    }
}

// MARK: - 计算Size
public extension String {
    // 获取获取字符串高度
    func getNormalHeight(strFont: UIFont, w: CGFloat) -> CGFloat {
        return getNormalSize(font: strFont, w: w, h: CGFloat.greatestFiniteMagnitude).height
    }

    // 获取字符串宽度
    func getNormalWidth(strFont: UIFont, h: CGFloat) -> CGFloat {
        return getNormalSize(font: strFont, w: CGFloat.greatestFiniteMagnitude, h: h).width
    }
    
    // 获取字符串的size
    private func getNormalSize(font: UIFont, w: CGFloat, h: CGFloat) -> CGSize {
        let strSize = self.boundingRect(with: CGSize(width: w, height: h), options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil).size
        return strSize
    }
    
    // 替换电话号码中间的4位数为****
    func encryptionPhoneNumber() -> String {
        if self.lengthOfBytes(using: .utf8) > 7 {
            var mutNumber = self
            let ceptionIndex = self.index(self.startIndex, offsetBy: 3)
            let offsetIndex = self.index(self.startIndex, offsetBy: 7)
            mutNumber.replaceSubrange(ceptionIndex ..< offsetIndex, with: "****")
            return mutNumber
        }
        return "****"
    }
}
