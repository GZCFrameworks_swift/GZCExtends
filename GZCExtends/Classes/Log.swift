//
//  Log.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

import Foundation

// MARK:- 输出
/// 条件编译的log 方法，release时不输出
/// - Parameters:
///   - message: 输出的内容
///   - file: 所在文件（不需要传）
///   - method: 所在方法（不需要传）
///   - line: 所在位置（行数，不需要传）
public func GZCLog<T>(_ message: T, filePath: String = #file, methodName: String = #function, line: Int = #line) {
    #if DEBUG
    let fileName = (filePath as NSString).lastPathComponent.components(separatedBy: ".").first!
    print("==>> \(fileName).\(methodName)[\(line)]: \(message) \n")
    #endif
}
