//
//  CGRectExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

@_exported import SwifterSwift
import CoreGraphics

// MARK: - 属性
public extension CGRect {

    /// 中心点坐标
    var center: CGPoint { CGPoint(x: midX, y: midY) }

}

// MARK: - 初始化
public extension CGRect {

    /// 由中心点和Size创建Rect
    /// - Parameters:
    ///   - center: 中心点
    ///   - size: Size
    init(center: CGPoint, size: CGSize) {
        let origin = CGPoint(x: center.x - size.width / 2.0, y: center.y - size.height / 2.0)
        self.init(origin: origin, size: size)
    }
}

// MARK: - 方法
public extension CGRect {

    /// 根据指定的锚点变换大小
    /// - Parameters:
    ///   - size: 目标大小
    ///   - anchor: 锚点
    ///     '(0, 0)'表示左上角，'(1, 1)' 表示右下角
    ///     默认为'(0.5, 0.5)'. 如:
    ///
    ///          anchor = CGPoint(x: 0.0, y: 1.0):
    ///
    ///                       A2------B2
    ///          A----B       |        |
    ///          |    |  -->  |        |
    ///          C----D       C-------D2
    ///
    func resizing(to size: CGSize, anchor: CGPoint = CGPoint(x: 0.5, y: 0.5)) -> CGRect {
        let sizeDelta = CGSize(width: size.width - width, height: size.height - height)
        return CGRect(origin: CGPoint(x: minX - sizeDelta.width * anchor.x,
                                      y: minY - sizeDelta.height * anchor.y),
                      size: size)
    }

}
