//
//  UIApplicationExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/11/1.
//

import UIKit

// MARK: - ViewController Stack
public extension UIApplication {
    
    /// 找到topViewController
    static var topViewController: UIViewController? {
        if topNavigationController != nil {
            return topNavigationController?.topViewController
        }
        if topTabbarController != nil {
            return topTabbarController?.selectedViewController
        }
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
            let presentedViewController = getTopPresentedViewController(rootVC)
            return presentedViewController
        }
        return nil
    }
    
    /// 找到最底层 UINavigationController
    static var navigationController: UINavigationController? {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
            if rootVC.isKind(of: UINavigationController.self) {
                return rootVC as? UINavigationController
            }
            if rootVC.isKind(of: UITabBarController.self) {
                let tabbarVC = rootVC as! UITabBarController
                if tabbarVC.selectedViewController!.isKind(of: UINavigationController.self) {
                    return tabbarVC.selectedViewController as? UINavigationController
                }
            }
        }
        return nil
    }
    
    /// 找到最顶层的 UITabBarController
    static var topNavigationController: UINavigationController? {
        if let rootTabVC = tabbarController {
            let presentedViewController = getTopPresentedViewController(rootTabVC)
            if presentedViewController is UINavigationController {
                return presentedViewController as? UINavigationController
            }
        }
        if let navVC = navigationController {
            let presentedViewController = getTopPresentedViewController(navVC)
            if presentedViewController is UINavigationController {
                return presentedViewController as? UINavigationController
            }
            return navVC
        }
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
            let presentedViewController = getTopPresentedViewController(rootVC)
            if presentedViewController is UINavigationController {
                return presentedViewController as? UINavigationController
            }
            if rootVC is UINavigationController {
                return rootVC as? UINavigationController
            }
        }
        return nil
    }
    
    /// 找到最底层 UITabBarController
    static var tabbarController: UITabBarController? {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
            if rootVC is UITabBarController {
                let tabbarVC = rootVC as! UITabBarController
                return tabbarVC
            }
        }
        return nil
    }
    
    /// 找到最顶层的 UITabBarController
    static var topTabbarController: UITabBarController? {
        if let rootTabVC = tabbarController {
            let presentedViewController = getTopPresentedViewController(rootTabVC)
            if presentedViewController is UITabBarController {
                return presentedViewController as? UITabBarController
            }
            return rootTabVC
        }
        return nil
    }
    
    /// 找到最后的 presentedViewController
    static func getTopPresentedViewController(_ forViewController: UIViewController) -> UIViewController {
        if forViewController.presentedViewController != nil {
            return getTopPresentedViewController(forViewController.presentedViewController!)
        }
        return forViewController
    }
    
    /// dismiss所有present出来的VIewController
    static func dismissAllPresentedViewController() {
        if let rootTabVC = tabbarController {
            let presentedViewController = getTopPresentedViewController(rootTabVC)
            if presentedViewController != rootTabVC {
                presentedViewController.dismiss(animated: true, completion: nil)
            }
        }
        if let navVC = navigationController {
            let presentedViewController = getTopPresentedViewController(navVC)
            if presentedViewController != navVC {
                presentedViewController.dismiss(animated: true, completion: nil)
            }
        }
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
            let presentedViewController = getTopPresentedViewController(rootVC)
            if presentedViewController != rootVC {
                presentedViewController.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
