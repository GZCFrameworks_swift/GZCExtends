//
//  ColorExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

@_exported import SwifterSwift

import SwifterSwift

public extension Color {
    
    /// 获取red色值
    var redComponent: CGFloat {
        var red: CGFloat = 0
        getRed(&red, green: nil , blue: nil, alpha: nil)
        return red
    }
    
    /// 获取green色值
    var greenComponent: CGFloat {
        var green: CGFloat = 0
        getRed(nil, green: &green , blue: nil, alpha: nil)
        return green
    }
    
    /// 获取blue色值
    var blueComponent: CGFloat {
        var blue: CGFloat = 0
        getRed(nil, green: nil , blue: &blue, alpha: nil)
        return blue
    }
    
    /// 获取透明度
    var alphaComponent: CGFloat {
        var alpha: CGFloat = 0
        getRed(nil, green: nil , blue: nil, alpha: &alpha)
        return alpha
    }

    /// 获取色调
    var hueComponent: CGFloat {
        var hue: CGFloat = 0
        getHue(&hue, saturation: nil, brightness: nil, alpha: nil)
        return hue
    }

    /// 获取饱和度
    var saturationComponent: CGFloat {
        var saturation: CGFloat = 0
        getHue(nil, saturation: &saturation, brightness: nil, alpha: nil)
        return saturation
    }

    /// 获取亮度
    var brightnessComponent: CGFloat {
        var brightness: CGFloat = 0
        getHue(nil, saturation: nil, brightness: &brightness, alpha: nil)
        return brightness
    }
    
    /// 获取颜色的UInt表示
    var uInt: UInt {
        let comps: [CGFloat] = {
            let comps = cgColor.components!
            return comps.count == 4 ? comps : [comps[0], comps[0], comps[0], comps[1]]
        }()

        var colorAsUInt32: UInt32 = 0
        colorAsUInt32 += UInt32(comps[0] * 255.0) << 16
        colorAsUInt32 += UInt32(comps[1] * 255.0) << 8
        colorAsUInt32 += UInt32(comps[2] * 255.0)

        return UInt(colorAsUInt32)
    }
    
    /// 获得互补色（可能为nil）。
    var complementary: Color? {
        let colorSpaceRGB = CGColorSpaceCreateDeviceRGB()
        let convertColorToRGBSpace: ((_ color: Color) -> Color?) = { color -> Color? in
            if self.cgColor.colorSpace!.model == CGColorSpaceModel.monochrome {
                let oldComponents = self.cgColor.components
                let components: [CGFloat] = [ oldComponents![0], oldComponents![0], oldComponents![0], oldComponents![1]]
                let colorRef = CGColor(colorSpace: colorSpaceRGB, components: components)
                let colorOut = Color(cgColor: colorRef!)
                return colorOut
            } else {
                return self
            }
        }
        
        let color = convertColorToRGBSpace(self)
        guard let componentColors = color?.cgColor.components else { return nil }
        let red: CGFloat = sqrt(pow(255.0, 2.0) - pow((componentColors[0]*255), 2.0))/255
        let green: CGFloat = sqrt(pow(255.0, 2.0) - pow((componentColors[1]*255), 2.0))/255
        let blue: CGFloat = sqrt(pow(255.0, 2.0) - pow((componentColors[2]*255), 2.0))/255
        return Color(red: red, green: green, blue: blue, alpha: 1.0)
    }
}


public extension Color {

    #if !os(watchOS)
    /// 根据系统模式(light/dark)生成不同的颜色（仅iOS13有效，iOS13以下直接生成light颜色）
    ///
    /// - Parameters:
    ///     - light: light/unspecified 主题下返回的颜色
    ///     - dark: dark 主题下返回的颜色
    convenience init(light: Color, dark: Color) {
        if #available(iOS 13.0, *) {
            self.init(dynamicProvider: {
                $0.userInterfaceStyle == .dark ? dark : light
            })
        } else {
            self.init(red: light.redComponent, green: light.greenComponent, blue: light.blueComponent, alpha: light.alphaComponent)
        }
    }
    #endif

}
