//
//  ViewController.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 10/19/2020.
//  Copyright (c) 2020 Guo ZhongCheng. All rights reserved.
//

import UIKit
import GZCExtends

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        let label = UILabel(text: "aaa", alignment: .left, numberOfLines: 0)
        
        let button = UIButton( title: "哈哈哈", titleColor: .white, backgroundColor: .systemBlue, cornerRadius: 15)
        button.frame = CGRect(x: 100, y: 100, width: 200, height: 30)
        view.addSubview(button)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

